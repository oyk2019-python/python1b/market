import requests

from stok import Depo, Urun
from satis import sat, satislar

erdal_depo = Depo()


def kdv(price, tax):
    price = float(price)
    tax = float(tax)
    return price + (price * tax)


while True:
    print("""
Erdal Market Defteri

1. Yeni Stok Ekle
2. Stoklari Listele
3. Satislar
4. Ürün Sat
5. Çıkış
    """)

    secim = int(input('Secim Yapiniz: '))

    if secim == 1:
        res = requests.get('http://167.71.7.181/products')
        for item in res.json():
            print("Urun: {}, {} TL".format(item['name'], item['price']))
            adet = int(input('Adet: '))
            if adet == 0:
                continue
            else:
                yeni_urun = Urun(
                    item['name'],
                    adet,
                    kdv(item['price'], item['tax']))
                erdal_depo.ekle(yeni_urun)
        erdal_depo.kaydet()
    elif secim == 3:
        satislar()
    elif secim == 2:
        erdal_depo.listele()
    elif secim == 4:
        erdal_depo.listele()
        isim = input('Almak istediginiz urun: ')
        adet = int(input('Adet: '))
        urun = erdal_depo.cikar(isim, adet)
        sat(urun, adet)
    elif secim == 5:
        break
    else:
        print("Hatali secim")
