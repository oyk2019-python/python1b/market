import csv
from datetime import datetime

def satislar():
    try:
        dosya = open('satis.csv', 'r')
        reader = csv.DictReader(dosya)
        for urun in reader:
            satis = float(urun['satis'])
            alis = float(urun['alis'])
            kar = (satis - alis) / alis * 100
            print(urun['isim'], kar)
    except FileNotFoundError:
        print("Satis yok")


def sat(urun, adet):
    satis_fiyati = urun.fiyat + 2.00
    satis_fiyati = satis_fiyati + (satis_fiyati * 0.18)
    try:
        dosya = open('satis.csv', 'r')
        dosya.close()
    except FileNotFoundError:
        dosya = open('satis.csv', 'w')
        writer = csv.writer(dosya)
        writer.writerow(['isim', 'adet', 'alis', 'satis', 'tarih'])
        dosya.close()
    finally:
        dosya = open('satis.csv', 'a')
        writer = csv.writer(dosya)
        writer.writerow([urun.isim, adet, urun.fiyat, satis_fiyati, datetime.now()])
        dosya.close()
