from .db import ekle, stoklar
from .urun import Urun

class Depo:
    stoklar = []

    def __init__(self):
        for urun in stoklar():
            yeni = Urun(urun['isim'],
                        urun['adet'],
                        urun['fiyat'])
            self.stoklar.append(yeni)

    def ekle(self, urun):
        urun_isimleri = map(lambda x: x.isim, self.stoklar)
        if urun.isim in urun_isimleri:
            for stok in self.stoklar:
                if urun.isim == stok.isim:
                    stok.adet += urun.adet
                    stok.fiyat = urun.fiyat
        else:
            self.stoklar.append(urun)

    def listele(self):
        for urun in self.stoklar:
            print(urun.isim, urun.adet, urun.fiyat)

    def cikar(self, urun_adi, adet):
        for urun in self.stoklar:
            if urun.isim == urun_adi:
                urun.adet -= adet
                self.kaydet()
                return urun

    def kaydet(self):
        ekle(self.stoklar)
