import os
import csv


def ekle(urunler):
    with open('stok.csv', 'w') as dosya:
            writer = csv.writer(dosya)
            writer.writerow(['isim', 'adet', 'fiyat'])
            for urun in urunler:
                writer.writerow([urun.isim, urun.adet, urun.fiyat])


def stoklar():
    try:
        dosya = open('stok.csv', 'r')
        reader = csv.DictReader(dosya)
        for urun in reader:
            yield urun
    except FileNotFoundError:
        dosya = open('stok.csv', 'w')
        writer = csv.writer(dosya)
        writer.writerow(['isim', 'adet', 'fiyat'])
    finally:
        dosya.close()
